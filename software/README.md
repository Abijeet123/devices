# Common platform for Shakti bare-metal drivers

Extended from the amazing https://github.com/michaeljclark/riscv-probe library!

# Getting Started
To copy and build the libfemto library, you can follow the following steps

    cd software
    mkdir build
    cd build
    cmake ..
    make

This should create a copy of libfemto.a in the 
